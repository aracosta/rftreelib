// TreeNode.h
// Author: Matteo Costa
// Copyright (C) 2015 Matteo Costa

#ifndef TreeNode_h
#define TreeNode_h

#include <RHGenericDriver.h>
#include "TreeBase.h"


#ifdef _GatewayLogic_INLINE
#define _INLINE_ inline
#else
#define _INLINE_
#endif

#define TN_LEAF_TABLE_LEN TN_MAX_DEVICES
#define TN_PROTO_VERSION	0x09
/////////////////////////////////////////////////////////////////////
/// \class RHDatagram RHDatagram.h <RHDatagram.h>
/// \brief Manager class for addressed, unreliable messages
///
/// Every RHDatagram node has an 8 bit address (defaults to 0).
/// Addresses (DEST and SRC) are 8 bit integers with an address of RH_BROADCAST_ADDRESS (0xff) 
/// reserved for broadcast.
///
/// \par Media Access Strategy
///
/// RHDatagram and the underlying drivers always transmit as soon as sendto() is called.
///
/// \par Message Lengths
///
/// Not all Radio drivers supported by RadioHead can handle the same message lengths. Some radios can handle
/// up to 255 octets, and some as few as 28. If you attempt to send a message that is too long for 
/// the underlying driver, sendTo() will return false and will not transmit the message. 
/// It is the programmers responsibility to make
/// sure that messages passed to sendto() do not exceed the capability of the radio. You can use the 
/// *_MAX_MESSAGE_LENGTH definitions or driver->maxMessageLength() to help.
///
/// \par Headers
///
/// Each message sent and received by a RadioHead driver includes 4 headers:
/// -TO The node address that the message is being sent to (broadcast RH_BROADCAST_ADDRESS (255) is permitted)
/// -FROM The node address of the sending node
/// -ID A message ID, distinct (over short time scales) for each message sent by a particilar node
/// -FLAGS A bitmask of flags.
class TreeNode : public TreeBase
{
public:

    /// Defines the structure of the RHRouter message header, used to keep track of end-to-end delivery parameters
    typedef struct
    {
	    uint8_t    dest;       ///< Destination node address
	    uint8_t    source;     ///< Originator node address
	    uint8_t    hops;       ///< Hops traversed so far
	    uint8_t    id;         ///< Originator sequence number
	    uint8_t    flags;      ///< Originator flags
	    // Data follows, Length is implicit in the overall message length
    } RoutedMessageHeader;

    /// Defines the structure of a RHRouter message
#define TN_ROUTER_MAX_MESSAGE_LEN (TN_MAX_MESSAGE_LEN - sizeof(TreeNode::RoutedMessageHeader))
    typedef struct
    {
	    RoutedMessageHeader header;    ///< end-to-end delivery header
	    uint8_t             data[TN_ROUTER_MAX_MESSAGE_LEN]; ///< Application payload data
    } RoutedMessage;

	typedef struct 
	{
		uint8_t			version;		// protocol version
		unsigned long	timestamp;
		uint8_t			rootDistance;
		// other to be defined		
	} BeaconMessage;

	typedef struct 
	{
		uint8_t			nodes[32] ;
	} NodeListMessage;
	
    /// Constructor. 
    /// \param[in] driver The RadioHead driver to use to transport messages.
    /// \param[in] thisAddress The address to assign to this node. Defaults to 0
    TreeNode(RHGenericDriver& driver, bool isRootNode = false);

    /// Initialise this instance and the 
    /// driver connected to it.
    bool init(void);
    
    /// Sends a message to the node(s) with the given address
    /// RH_BROADCAST_ADDRESS is a valid address which will cause the message
    /// to be accepted by all RHDatagram nodes within range.
    /// \param[in] buf Pointer to the binary message to send
    /// \param[in] len Number of octets to send (> 0)
    /// \param[in] address The address to send the message to.
    /// \return true if the message not too loing fot eh driver, and the message was transmitted.
	bool sendto(uint8_t* buf, uint8_t len, uint8_t dest, uint8_t flags = TN_PACKET_TYPE_USMG);

    /// Turns the receiver on if it not already on.
    /// If there is a valid message available for this node, copy it to buf and return true
    /// The SRC address is placed in *from if present and not NULL.
    /// The DEST address is placed in *to if present and not NULL.
    /// If a message is copied, *len is set to the length.
    /// You should be sure to call this function frequently enough to not miss any messages
    /// It is recommended that you call it in your main loop.
    /// \param[in] buf Location to copy the received message
    /// \param[in,out] len Pointer to available space in buf. Set to the actual number of octets copied.
    /// \param[in] from If present and not NULL, the referenced uint8_t will be set to the FROM address
    /// \param[in] to If present and not NULL, the referenced uint8_t will be set to the TO address
    /// \param[in] id If present and not NULL, the referenced uint8_t will be set to the ID
    /// \param[in] flags If present and not NULL, the referenced uint8_t will be set to the FLAGS
    /// (not just those addressed to this node).
    /// \return true if a valid message was copied to buf
    bool recvfrom(uint8_t* buf, uint8_t* len, uint8_t* from = NULL, uint8_t* to = NULL, uint8_t* id = NULL, uint8_t* flags = NULL);
    
    // print the internal state for debugging
    void printState(void);
    void printLogicState(void);
    
    uint8_t isRegistered(void) { return (actualState == rf_gateway_logic_registered) ; };
    

protected:
    typedef enum {
        rf_gateway_logic_wait_master_beam,
        rf_gateway_logic_register_to_master,
        rf_gateway_logic_wait_any_beam,
        rf_gateway_logic_register_to_gateway,
        rf_gateway_logic_registered
    } rf_gateway_logic_state_id;
    
	/// 
    bool route(RoutedMessage* message, uint8_t messageLen);


	/// Check if node is in the leaf list
	/// \return true if in list
	bool isLeaf(uint8_t node);

	/// add node to the leaf list
	void setLeaf(uint8_t node);

	/// update the leaf table with data from array
	/// modify the _leafTable ONLY if the sender is one of our leaf node
	/// if bit is set associate the destination with the sender
	/// if bit is clear remove association ONLY if it was associated to sender
	/// \param[in] sender The address of the update sender
	/// \param[in] newTable 32 byte bitmap of node that are leaf for the sender
	void updateLeaf(uint8_t sender, uint8_t *newTable);

	/// remove node from the leaf list
	void clearLeaf(uint8_t node);

	/// Check if any leaf registered to know if I'm a gateway
	/// \return true if node is a gateway
	bool isGateway(void);

    ///
	uint8_t getRoute(uint8_t to);
	
	///
	void removeRoutes(uint8_t gateway);

    inline bool isRoot() {return _thisAddress==0?true:false;}
private:
    /// Temporary mesage buffer
    static RoutedMessage _tmpMessage;

	/// array of leaf node connected to us
	/// table indexed by destination address
	/// every entry contain the address of the gateway to the destination
	/// 255 - not reachable, send messages to upper gateway
	/// my address - direct leaf
	/// other - gateway under me to reach the destination
	uint8_t _leaf_table[TN_LEAF_TABLE_LEN];

    /// timer for waiting the root beam
    unsigned long _upperBeamTimeoutLastTimer;
    uint8_t _upperBeamTimeoutActive;
    
    /// timer for registration
    unsigned long _registrationTimeoutLastTimer;
    uint8_t _registrationTimeoutActive;
    
    /// timer for sending beam when registered to a gateway or root
    unsigned long _gatewayBeamLastTimer;
    uint8_t _gatewayBeamTimeoutActive;
    
    uint8_t _GatewayId;    // keep the id of our gateway
    uint8_t _RootDistance; // number of hop to the master - 1 for directly connected, higher if more gateway
    
    /////////////////////////////////
    // init the logic engine
    void  _INLINE_ rf_gateway_logic_init(void);
    
    // function triggered by timers
    void  _INLINE_ beacon_timeout_trg(void);
    void  _INLINE_ registration_timeout_trg(void);
    
    // function triggered by received messages
    void  _INLINE_ root_beacon_received_trg(void);
    void  _INLINE_ gateway_beacon_received_trg(void);
    void  _INLINE_ registration_ACK_trg(void);
    void  _INLINE_ registration_NAK_trg(void);
    void  _INLINE_ registration_update_trg(void);
    void  _INLINE_ registration_request_trg(void);
    void _INLINE_ gateway_beacon_send_trg(void);
    //
    void  _INLINE_ any_communcation_trg(void);
    void  _INLINE_ gateway_communication_trg(void);
    void  _INLINE_ local_communication_trg(void);
    
    rf_gateway_logic_state_id actualState;
    
    // function called from the GatewayLogic for various event
    void _INLINE_ beacon_timeout_start_evt(void);
    void _INLINE_ beacon_timeout_stop_evt(void);
    void _INLINE_ device_registered_evt(void);
    void _INLINE_ device_unregistered_evt(void);
    void _INLINE_ device_initialized_evt(void);
    void _INLINE_ register_timeout_start_evt(void);
    void _INLINE_ register_timeout_stop_evt(void);
    void _INLINE_ send_registration_request_evt(void);
    void _INLINE_ send_registration_ACK_evt(void);
    void _INLINE_ send_registration_NACK_evt(void);
    void _INLINE_ send_registration_update_evt(void);
    void _INLINE_ process_registration_update_evt(void);
    void _INLINE_ process_message_evt(void) {};
    void _INLINE_ process_gateway_evt(void) {};
    void _INLINE_ gateway_beacon_evt(void);
    void _INLINE_ gateway_beacon_timeout_stop_evt(void);
    void _INLINE_ gateway_beacon_timeout_start_evt(void);
};

#ifdef _GatewayLogic_INLINE
#include "GatewayLogic.h"
#endif

#endif
