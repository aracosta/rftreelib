// TreeRoot.cpp
//
// Define TreeRoot datagram protocol
//
// Author: Matteo Costa
// Copyright (C) 2015 Matteo Costa
#if 0
#include "TreeRoot.h"


TreeRoot::TreeRoot(RHGenericDriver& driver) : TreeBase(driver, 0)
{
    _beamActive = false;
}

////////////////////////////////////////////////////////////////////
// Public methods

void TreeRoot::printState()
{
    unsigned long now = millis();

    Serial.println(F("TreeRoot state:"));
    Serial.print(F("   NodeId:"));
    Serial.println(thisAddress());
    if (_beamActive) {
        Serial.print(F("   BeamTimer state: active - "));
        Serial.print(F("   BeamTimer count: "));
        Serial.println((TN_GLOBAL_BEAM_TIMEOUT/2) - (now - _beamLastTimer));
    } else {
        Serial.println(F("   BeamTimer: inactive"));
    }
    TreeBase::printState();
    return;
}

bool TreeRoot::init()
{
    bool ret = TreeBase::init();
    if (ret)
    {
        beacon_timeout_start_evt();
        // send an RNAK at startup
        sendto(NULL, 0, RH_BROADCAST_ADDRESS, TN_PACKET_TYPE_RNAK);
    }
    return ret;
}

bool TreeRoot::sendto(uint8_t* buf, uint8_t len, uint8_t address)
{
    return sendto(buf, len, address, TN_PACKET_TYPE_USMG);
}

bool TreeRoot::sendto(uint8_t* buf, uint8_t len, uint8_t address, uint8_t pkt_type)
{
    return sendto(buf, len, address, pkt_type);
}

////////////////////////////////////////////////////////////////////
bool TreeRoot::recvfrom(uint8_t* buf, uint8_t* len, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
    uint8_t _from;
    uint8_t _to;
    uint8_t _id;
    uint8_t _flags;
    uint8_t _len;
    
    // process timers and timeouts
    unsigned long now = millis();
    if (_beamActive && (now - _beamLastTimer) > (TN_ROOT_BEAM_FREQ)) {
        root_beacon_evt();
        beacon_timeout_start_evt();
    }
    
    _len = TN_MAX_MESSAGE_LEN;
    // Get the message before its clobbered by the ACK (shared rx and tx buffer in RH
    if (TreeBase::recvfrom((uint8_t*)&_tmpMessage, &_len, &_from, &_to, &_id, &_flags ))
    {      
        // check if Service message
        if ((_flags & TN_PACKET_TYPE_MASK) != TN_PACKET_TYPE_USMG)
        {
            if (_to == _thisAddress)
            {
                if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RREQ)
                {
                    setLeaf(headerFrom());
                    sendto(NULL, 0, headerFrom(), TN_PACKET_TYPE_RACK);
                }
                if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RUPD)
                {
                    Serial.println(F("process_registration_update_evt"));
                    updateLeaf( headerFrom(), _msgBuffer);
                }
            }
            
        }
        // check if for us and deliver to application
        else if ( _to == _thisAddress || _to == RH_BROADCAST_ADDRESS)
        {
            // its a new message so process it
            if (from)  *from =  _from;
            if (to)    *to =    _to;
            if (id)    *id =    _id;
            if (flags) *flags = _flags;

            if (*len > TN_MAX_MESSAGE_LEN) {
                return false;
            }
            if (*len < _msgLen) {
                _msgLen = *len;
            }
            memcpy(buf, _msgBuffer, _msgLen);
            *len = _msgLen;
            return true;
        }
    }
    // No message available for the application
    return false;
}

/*
bool TreeRoot::recvfromAckTimeout(uint8_t* buf, uint8_t* len, uint16_t timeout, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
    unsigned long starttime = millis();
    while ((millis() - starttime) < timeout)
    {
        if (recvfromAck(buf, len, from, to, id, flags))
            return true;
        YIELD;
    }
    return false;
}
*/

/////////////////////////////// gateway logic function //////////
// called when need to send a beacon message
void TreeRoot::root_beacon_evt(void)
{
    unsigned long now = millis();
    uint8_t payload_message[4];
    
    //Serial.println(F("root_beacon_evt"));
    payload_message[0] = now>>24 & 0xff;
    payload_message[1] = now>>16 & 0xff;
    payload_message[2] = now>>8 & 0xff;
    payload_message[3] = now & 0xff;
    sendto(payload_message, 4, RH_BROADCAST_ADDRESS, TN_PACKET_TYPE_BCN);
}

// called when master beam not received
void TreeRoot::beacon_timeout_start_evt(void)
{
    //Serial.println(F("beacon_timeout_start_evt"));
    _beamLastTimer = millis();
    _beamActive = true;
}

// called when need to stop master beam timeout
void TreeRoot::beacon_timeout_stop_evt()
{
    //Serial.println(F("beacon_timeout_stop_evt"));
    _beamActive = false;
}

#endif