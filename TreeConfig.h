//
// File			TreeConfig.h
// Header
//
// Details		<#details#>
//	
// Project		 rf-distrisense
// Developed with [embedXcode](http://embedXcode.weebly.com)
// 
// Author		Matteo Costa
// 				Matteo Costa
//
// Date			03/06/15 14:17
// Version		<#version#>
// 
// Copyright	© Matteo Costa, 2015
// Licence    <#license#>
//
// See			ReadMe.txt for references
//


#ifndef TreeConfig_h
#define TreeConfig_h


#ifdef _DEBUG
#define debugln(A)  Serial.println((A))
#define debug(A)  Serial.print((A))
#else
#define debugln(A)
#define debug(A)
#endif


#define TN_MAX_DEVICES  32


// This is the maximum possible message size.
#define TN_MAX_MESSAGE_LEN  36

#define TN_DEFAULT_RETRIES 3
#define TN_DEFAULT_TIMEOUT 1000

// The acknowledgement bit in the FLAGS
#define TN_FLAG_RESET 0xFF
#define TN_FLAGS_ACK 0x80

#define TN_FLAGS_PACKET_TYPE_MASK 0x70
#define TN_FLAGS_DIRECTION_MASK 0x08
#define TN_FLAGS_ROOT_DISTANCE_MASK 0x07

// Bit values for direction flag
#define TN_FLAGS_DIRECTION_LEAF 0x08
#define TN_FLAGS_DIRECTION_ROOT 0x00

// Bit for Packet Type field in the FLAGS
#define TN_PACKET_TYPE_MASK 0x0f
#define TN_PACKET_TYPE_BCN  0x01
#define TN_PACKET_TYPE_RREQ 0x02
#define TN_PACKET_TYPE_RACK 0x03
#define TN_PACKET_TYPE_RNAK 0x04
#define TN_PACKET_TYPE_RUPD 0x05
#define TN_PACKET_TYPE_USMG 0x0e


#define TN_GATEWAY_NONE			RH_BROADCAST_ADDRESS
#define TN_NOT_LEAF				RH_BROADCAST_ADDRESS
#define TN_NO_ROOT_DISTANCE		RH_BROADCAST_ADDRESS
#define TN_GATEWAY_ROOT			0
#define TN_MAX_ROOT_DISTANCE	7

#define TN_GLOBAL_BEAM_TIMEOUT 60000
#define TN_ROOT_BEAM_FREQ		(TN_GLOBAL_BEAM_TIMEOUT/3)
#define TN_GLOBAL_REGISTER_TIMEOUT 10000


#endif
