// TreeNode.h
// Author: Matteo Costa
// Copyright (C) 2015 Matteo Costa

#ifndef TreeBase_h
#define TreeBase_h

#include <RHGenericDriver.h>
#include "TreeConfig.h"

/////////////////////////////////////////////////////////////////////
/// \class RHDatagram RHDatagram.h <RHDatagram.h>
/// \brief Manager class for addressed, unreliable messages
///
/// Every RHDatagram node has an 8 bit address (defaults to 0).
/// Addresses (DEST and SRC) are 8 bit integers with an address of RH_BROADCAST_ADDRESS (0xff) 
/// reserved for broadcast.
///
/// \par Media Access Strategy
///
/// RHDatagram and the underlying drivers always transmit as soon as sendto() is called.
///
/// \par Message Lengths
///
/// Not all Radio drivers supported by RadioHead can handle the same message lengths. Some radios can handle
/// up to 255 octets, and some as few as 28. If you attempt to send a message that is too long for 
/// the underlying driver, sendTo() will return false and will not transmit the message. 
/// It is the programmers responsibility to make
/// sure that messages passed to sendto() do not exceed the capability of the radio. You can use the 
/// *_MAX_MESSAGE_LENGTH definitions or driver->maxMessageLength() to help.
///
/// \par Headers
///
/// Each message sent and received by a RadioHead driver includes 4 headers:
/// -TO The node address that the message is being sent to (broadcast RH_BROADCAST_ADDRESS (255) is permitted)
/// -FROM The node address of the sending node
/// -ID A message ID, distinct (over short time scales) for each message sent by a particilar node
/// -FLAGS A bitmask of flags. The most significant 4 bits are reserved for use by RadioHead. The least
/// significant 4 bits are reserved for applications.
class TreeBase
{
public:

    /// Constructor. 
    /// \param[in] driver The RadioHead driver to use to transport messages.
    /// \param[in] thisAddress The address to assign to this node. Defaults to 0
    TreeBase(RHGenericDriver& driver);

    /// Initialise this instance and the 
    /// driver connected to it.
    bool init();

    /// Sets the address of this node. Defaults to 0. 
    /// This will be used to set the FROM address of all messages sent by this node.
    /// In a conventional multinode system, all nodes will have a unique address 
    /// (which you could store in EEPROM).
    /// \param[in] thisAddress The address of this node
    void setThisAddress(uint8_t thisAddress);

    /// Sets the minimum retransmit timeout. If sendtoWait is waiting for an ack
    /// longer than this time (in milliseconds),
    /// it will retransmit the message. Defaults to 200ms. The timeout is measured from the end of
    /// transmission of the message. It must be at least longer than the the transmit
    /// time of the acknowledgement (preamble+6 octets) plus the latency/poll time of the receiver.
    /// For fast modulation schemes you can considerably shorten this time.
    /// The actual timeout is randomly varied between timeout and timeout*2.
    /// \param[in] timeout The new timeout period in milliseconds
    void setTimeout(uint16_t timeout);
    
    /// Sets the maximum number of retries. Defaults to 3 at construction time.
    /// If set to 0, each message will only ever be sent once.
    /// sendtoWait will give up and return false if there is no ack received after all transmissions time out
    /// and the retries count is exhausted.
    /// param[in] retries The maximum number a retries.
    void setRetries(uint8_t retries);
    
    /// Returns the currently configured maximum retries count.
    /// Can be changed with setRetries().
    /// \return The currently configured maximum number of retries.
    uint8_t retries();
    
    /// Tests whether a new message is available
    /// from the Driver.
    /// On most drivers, this will also put the Driver into RHModeRx mode until
    /// a message is actually received bythe transport, when it will be returned to RHModeIdle.
    /// This can be called multiple times in a timeout loop.
    /// \return true if a new, complete, error-free uncollected message is available to be retreived by recv()
    bool            available();

    /// Starts the Driver receiver and blocks until a valid received 
    /// message is available.
    void            waitAvailable();

    /// Blocks until the transmitter 
    /// is no longer transmitting.
    bool            waitPacketSent();

    /// Blocks until the transmitter is no longer transmitting.
    /// or until the timeout occuers, whichever happens first
    /// \param[in] timeout Maximum time to wait in milliseconds.
    /// \return true if the radio completed transmission within the timeout period. False if it timed out.
    bool            waitPacketSent(uint16_t timeout);

    /// Starts the Driver receiver and blocks until a received message is available or a timeout
    /// \param[in] timeout Maximum time to wait in milliseconds.
    /// \return true if a message is available
    bool            waitAvailableTimeout(uint16_t timeout);

    /// Sets the TO header to be sent in all subsequent messages
    /// \param[in] to The new TO header value
    void           setHeaderTo(uint8_t to);

    /// Sets the FROM header to be sent in all subsequent messages
    /// \param[in] from The new FROM header value
    void           setHeaderFrom(uint8_t from);

    /// Sets the ID header to be sent in all subsequent messages
    /// \param[in] id The new ID header value
    void           setHeaderId(uint8_t id);

    /// Sets and clears bits in the FLAGS header to be sent in all subsequent messages
    /// \param[in] set bitmask of bits to be set
    /// \param[in] clear bitmask of flags to clear
    void           setHeaderFlags(uint8_t set, uint8_t clear = RH_FLAGS_NONE);

    /// Returns the TO header of the last received message
    /// \return The TO header of the most recently received message.
    uint8_t        headerTo();

    /// Returns the FROM header of the last received message
    /// \return The FROM header of the most recently received message.
    uint8_t        headerFrom();

    /// Returns the ID header of the last received message
    /// \return The ID header of the most recently received message.
    uint8_t        headerId();

    /// Returns the FLAGS header of the last received message
    /// \return The FLAGS header of the most recently received message.
    uint8_t        headerFlags();

    /// Returns the address of this node.
    /// \return The address of this node
    uint8_t         thisAddress();

    /// Returns the number of retransmissions
    /// we have had to send since starting or since the last call to resetRetransmissions().
    /// \return The number of retransmissions since initialisation.
    uint32_t retransmissions();

    /// Resets the count of the number of retransmissions
    /// to 0.
    void resetRetransmissions();

protected:
    /// Sends a message to the node(s) with the given address
    /// RH_BROADCAST_ADDRESS is a valid address which will cause the message
    /// to be accepted by all RHDatagram nodes within range.
    /// \param[in] buf Pointer to the binary message to send
    /// \param[in] len Number of octets to send (> 0)
    /// \param[in] address The address to send the message to.
    /// \param[in] pkt_type The packet type to put in FLAGS
    /// \return true if the message not too long and the message was queued.
    bool sendto(uint8_t* buf, uint8_t len, uint8_t address, uint8_t pkt_type);
    
    /// Repeat the message in _msgBuffer using the same TO, FROM and ID
    /// FLAGS are changed to reflect the new RootDistance and Direction
    /// \return true if the message was queued.
    //bool repeat(uint8_t flags);

    /// Turns the receiver on if it not already on.
    /// If there is a valid message available for this node, check if it is for this node
    /// and copy it to _msgBuffer and return true
    /// If not for this node check if it need to be forwarded and resend it
    /// If a message is copied, _msgLen is set to the length.
    /// You should be sure to call this function frequently enough to not miss any messages
    /// \return true if a valid message was copied to _msgBuffer
    bool recvfrom(uint8_t* buf, uint8_t* len, uint8_t* from = NULL, uint8_t* to = NULL, uint8_t* id = NULL, uint8_t* flags = NULL);

    // print the internal state for debugging
    void printState();
    
    uint8_t _seenIds[TN_MAX_DEVICES];
    
    /// Count of retransmissions we have had to send
    uint32_t _retransmissions;
    
    /// The last sequence number to be used
    /// Defaults to 0
    uint8_t _lastSequenceNumber;
    
    // Retransmit timeout (milliseconds)
    /// Defaults to 200
    uint16_t _timeout;
    
    // Retries (0 means one try only)
    /// Defaults to 3
    uint8_t _retries;
    
    /// The Driver we are to use
    RHGenericDriver&    _driver;

    /// The address of this node
    uint8_t         _thisAddress;
    
    
private:
    //uint8_t _msgBuffer[TN_MAX_MESSAGE_LEN];
    //uint8_t _msgLen;
    
    /// Enqueue an Ack message.
    /// Ack message has the
    void acknowledge(uint8_t from, uint8_t to, uint8_t id, uint8_t flags);

    /// Send the message in the TxQueue and flag as transmitted
    /// \return true if message sent, false if error or nothing to send
    void processTx();
    
    void printDataReceived(uint8_t *buf, uint8_t len);
    void printDataSent(void);

	typedef enum
	{
		empty = 0,		// no message
		queued,			// queued by someone
		waiting,		// delaying tx
		sent			// sent and waiting ack
	} TxQueueState;

	#define TN_MAX_TXQUEUE_LEN 4
	typedef struct
	{
		TxQueueState _state;  // message sent waiting for Ack
		uint8_t _len;
		uint8_t _To;
		uint8_t _From;
		uint8_t _Id;
		uint8_t _Flags;
		uint8_t _payload[TN_MAX_MESSAGE_LEN];
	} S_TxQueue;

	typedef struct
	{
		TxQueueState _state;  // message sent waiting for Ack
		uint8_t _To;
		uint8_t _From;
		uint8_t _Id;
		uint8_t _Flags;
	} S_TxAck;
	


    unsigned long _delayTxMillis;
	unsigned long _delayTxTimeout;	// randomly generated
    unsigned long _sentMillis;
    uint8_t _retriesDone;
    S_TxQueue _TxQueue[TN_MAX_TXQUEUE_LEN];
    uint8_t _TxQueueHead;
    uint8_t _TxQueueTail;


};

#endif
