

#ifdef _GatewayLogic_INLINE
#define _INLINE_ inline
#else
#define _INLINE_
#endif

#define _logic_debug_enable
#ifdef _logic_debug_enable
#define logic_debugln(A)  Serial.println((A))
#define logic_debug(A)  Serial.print((A))
#else
#define logic_debugln(A)
#define logic_debug(A)
#endif

void _INLINE_ TreeNode::rf_gateway_logic_init(void)
{
    logic_debugln(F("rf_gateway_logic_init"));
    if (isRoot())
    {
		gateway_beacon_timeout_start_evt();
    } else {
		beacon_timeout_start_evt();
	}
	device_initialized_evt();
}

void _INLINE_ TreeNode::root_beacon_received_trg(void)
{
    logic_debugln(F("root_beacon_received_trg"));
    switch(actualState)
    {
        case rf_gateway_logic_wait_master_beam:
            beacon_timeout_stop_evt();
            actualState = rf_gateway_logic_register_to_master;
            register_timeout_start_evt();
            send_registration_request_evt();
            break;
        case rf_gateway_logic_wait_any_beam:
            actualState = rf_gateway_logic_register_to_master;
            register_timeout_start_evt();
            send_registration_request_evt();
            break;
        case rf_gateway_logic_registered:
            beacon_timeout_start_evt();
            break;
        default:
            break;
    }
}

void _INLINE_ TreeNode::gateway_beacon_received_trg(void)
{
    logic_debugln(F("gateway_beacon_received_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_wait_any_beam:
            actualState = rf_gateway_logic_register_to_gateway;
            register_timeout_start_evt();
            send_registration_request_evt();
            break;
        case rf_gateway_logic_registered:
            beacon_timeout_start_evt();
            break;
        default:
            break;
    }
}


void _INLINE_ TreeNode::gateway_beacon_send_trg(void)
{
    logic_debugln(F("gateway_beacon_send_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_registered:
            gateway_beacon_evt();
            gateway_beacon_timeout_start_evt();
            break;
        default:
            break;
    }
}


void _INLINE_ TreeNode::beacon_timeout_trg(void)
{
    logic_debugln(F("beacon_timeout_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_wait_master_beam:
            beacon_timeout_stop_evt();
            actualState = rf_gateway_logic_wait_any_beam;
            break;
        case rf_gateway_logic_registered:
            actualState = rf_gateway_logic_wait_master_beam;
            beacon_timeout_start_evt();
            gateway_beacon_timeout_stop_evt();
            device_unregistered_evt();
            break;
        default:
            break;
    }
}

void _INLINE_ TreeNode::registration_ACK_trg(void)
{
    logic_debugln(F("registration_ACK_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_register_to_master:
            register_timeout_stop_evt();
            beacon_timeout_start_evt();
            actualState = rf_gateway_logic_registered;
            device_registered_evt();
            gateway_beacon_timeout_start_evt();
            break;
        case rf_gateway_logic_register_to_gateway:
            register_timeout_stop_evt();
            beacon_timeout_start_evt();
            actualState = rf_gateway_logic_registered;
            device_registered_evt();
            gateway_beacon_timeout_start_evt();
            break;
        default:
            break;
    }
}

void _INLINE_ TreeNode::registration_NAK_trg(void)
{
    logic_debugln(F("registration_NAK_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_register_to_gateway:
            register_timeout_stop_evt();
            beacon_timeout_start_evt();
            actualState = rf_gateway_logic_wait_any_beam;
            break;
        case rf_gateway_logic_registered:
            actualState = rf_gateway_logic_wait_master_beam;
            beacon_timeout_start_evt();
            gateway_beacon_timeout_stop_evt();
            device_unregistered_evt();
            break;
        default:
            break;
    }
}

void _INLINE_ TreeNode::registration_request_trg(void)
{
    logic_debugln(F("registration_request_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_registered:
            send_registration_ACK_evt();
            send_registration_update_evt();
            break;
        default:
            break;
    }
}


void _INLINE_ TreeNode::registration_update_trg(void)
{
    logic_debugln(F("registration_update_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_registered:
            process_registration_update_evt();
            send_registration_update_evt();
            break;
        default:
            break;
    }
}

void _INLINE_ TreeNode::registration_timeout_trg(void)
{
    logic_debugln(F("registration_timeout_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_register_to_master:
            register_timeout_stop_evt();
            actualState = rf_gateway_logic_wait_master_beam;
            beacon_timeout_start_evt();
            gateway_beacon_timeout_stop_evt();
            device_unregistered_evt();
            break;
        case rf_gateway_logic_register_to_gateway:
            register_timeout_stop_evt();
            actualState = rf_gateway_logic_wait_any_beam;
            break;
        default:
            break;
    }
}

void _INLINE_ TreeNode::gateway_communication_trg(void)
{
    logic_debugln(F("gateway_communication_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_registered:
            process_gateway_evt();
            break;
        default:
            break;
    }
}

void _INLINE_ TreeNode::local_communication_trg(void)
{
    logic_debugln(F("local_communication_trg"));
    
    switch(actualState)
    {
        case rf_gateway_logic_registered:
            process_message_evt();
            break;
        default:
            break;
    }
}

// called when master beam not received
void _INLINE_ TreeNode::beacon_timeout_start_evt(void)
{
    logic_debugln(F("beacon_timeout_start_evt"));
    _upperBeamTimeoutLastTimer = millis();
    _upperBeamTimeoutActive = true;
}

// called when need to stop master beam timeout
void _INLINE_ TreeNode::beacon_timeout_stop_evt()
{
    logic_debugln(F("beacon_timeout_stop_evt"));
    _upperBeamTimeoutActive = false;
}


// called to start the gateway beam sending
void _INLINE_ TreeNode::gateway_beacon_timeout_start_evt(void)
{
    logic_debugln(F("gateway_beacon_timeout_start_evt"));
    _gatewayBeamLastTimer = millis();
    _gatewayBeamTimeoutActive = true;
}

// called when need to stop master beam timeout
void _INLINE_ TreeNode::gateway_beacon_timeout_stop_evt()
{
    logic_debugln(F("gateway_beacon_timeout_stop_evt"));
    _gatewayBeamTimeoutActive = false;
}

// called when registration timeout expired
void _INLINE_ TreeNode::register_timeout_start_evt()
{
    logic_debugln(F("register_timeout_start_evt"));
    _registrationTimeoutLastTimer = millis();
    _registrationTimeoutActive = true;
}
// called when need to stop registration timeout
void _INLINE_ TreeNode::register_timeout_stop_evt()
{
    logic_debugln(F("register_timeout_stop_evt"));
    _registrationTimeoutActive = false;
}

// called when received RACK from master or gateway
void _INLINE_ TreeNode::device_registered_evt()
{
	BeaconMessage *_tmp;
    _tmp = (BeaconMessage *)&_tmpMessage;

    logic_debugln(F("device_registered_evt"));
    _RootDistance = _tmp->rootDistance;
    _GatewayId = headerFrom();
}

// called when received an RNAK or beacon timeout
void _INLINE_ TreeNode::device_unregistered_evt()
{
    logic_debugln(F("device_unregistered_evt"));
    if (isGateway()) {
        TreeBase::sendto(NULL, 0, RH_BROADCAST_ADDRESS, TN_PACKET_TYPE_RNAK);
    }
    _RootDistance = TN_NO_ROOT_DISTANCE;
    _GatewayId = TN_GATEWAY_NONE;
}

// called at startup to notify other of the start or restart
void _INLINE_ TreeNode::device_initialized_evt()
{
	logic_debugln(F("device_initialized_evt"));
	TreeBase::sendto(NULL, 0, RH_BROADCAST_ADDRESS, TN_PACKET_TYPE_RNAK);
}

// send a RREQ to the remote node the message came from
void _INLINE_ TreeNode::send_registration_request_evt()
{
    logic_debugln(F("send_registration_request_evt"));
    TreeBase::sendto(NULL, 0, headerFrom(), TN_PACKET_TYPE_RREQ) ;
}

// send the new leaf mask to the gateway
void _INLINE_ TreeNode::process_registration_update_evt()
{
    logic_debugln(F("process_registration_update_evt"));
    updateLeaf(headerFrom(), (uint8_t*)&_tmpMessage);
}

// send an ACK reply to the node requesting the registration and
void _INLINE_ TreeNode::send_registration_ACK_evt()
{
    logic_debugln(F("send_registration_ACK_evt"));
    setLeaf(headerFrom());
    TreeBase::sendto(NULL, 0, headerFrom(), TN_PACKET_TYPE_RACK);
}

// send NACK replay to requester
void _INLINE_ TreeNode::send_registration_NACK_evt()
{
    logic_debugln(F("send_registration_NACK_evt"));
    TreeBase::sendto(NULL, 0, headerFrom(), TN_PACKET_TYPE_RNAK);
}

// send a beacon for other to connect to us
void _INLINE_ TreeNode::gateway_beacon_evt(void)
{
    unsigned long now = millis();
	BeaconMessage *_tmp;
    _tmp = (BeaconMessage *)&_tmpMessage;
	
    logic_debugln(F("gateway_beacon_evt"));
	_tmp->version = TN_PROTO_VERSION;
    _tmp->timestamp = now;
	_tmp->rootDistance = _RootDistance;
    TreeBase::sendto((uint8_t*)_tmp, sizeof(BeaconMessage), RH_BROADCAST_ADDRESS, TN_PACKET_TYPE_BCN);
}


// send the new leaf mask to the gateway
void _INLINE_ TreeNode::send_registration_update_evt()
{
	uint8_t *_tmp;
	_tmp = (uint8_t *)&_tmpMessage;

    logic_debugln(F("send_registration_update_evt"));
    for (uint8_t i = 0; i < TN_LEAF_TABLE_LEN; i++) {
        if (_leaf_table[i] != TN_NOT_LEAF) {
            _tmp[i/8] |= (1<<(i%8)) ;
        } else {
            _tmp[i/8] &= ~(1<<(i%8));
        }
    }
    TreeBase::sendto(_tmp, (TN_LEAF_TABLE_LEN/8)+1, _GatewayId, TN_PACKET_TYPE_RUPD);

}
