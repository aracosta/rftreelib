// TreeNode.cpp
//
// Define TreeNode datagram protocol
//
// Author: Matteo Costa
// Copyright (C) 2015 Matteo Costa

#define _DEBUG

#include "TreeNode.h"

TreeNode::RoutedMessage TreeNode::_tmpMessage;

TreeNode::TreeNode(RHGenericDriver& driver, bool isRootNode)
: TreeBase(driver)
{
	if (isRootNode)	// I'm root
	{
		_thisAddress = 0;
		_GatewayId = 0;
		_RootDistance = 0;
		_registrationTimeoutActive = false;
		_upperBeamTimeoutActive = false;
		_gatewayBeamLastTimer = false;
		actualState = rf_gateway_logic_registered;
	} else {
		_thisAddress = 1;		// set a default != 0
		_GatewayId = TN_GATEWAY_NONE;
		_RootDistance = TN_NO_ROOT_DISTANCE;
		_registrationTimeoutActive = false;
		_upperBeamTimeoutActive = false;
		_gatewayBeamLastTimer = false;
		actualState = rf_gateway_logic_wait_master_beam;
	}
    for (uint8_t i = 0; i<TN_LEAF_TABLE_LEN; i++) {
	    _leaf_table[i] = TN_NOT_LEAF;
    }

}

////////////////////////////////////////////////////////////////////
// Public methods

bool TreeNode::init()
{
    bool ret = TreeBase::init();
    if (ret)
    {
        rf_gateway_logic_init();
    }
    return ret;
}

////////////////////////////////////////////////////////////////////
// Waits for delivery to the next hop (but not for delivery to the final destination)
bool TreeNode::sendto(uint8_t* buf, uint8_t len, uint8_t dest, uint8_t flags)
{
    // must check if connected to a root or gateway
    if (_GatewayId == TN_GATEWAY_NONE) {
	    return false;
    }

	if (((uint16_t)len + sizeof(RoutedMessageHeader)) > _driver.maxMessageLength())
		return false;
	// Construct a RH RouterMessage message
	_tmpMessage.header.source = thisAddress();
	_tmpMessage.header.dest = dest;
	_tmpMessage.header.hops = 0;
	_tmpMessage.header.id = 0; //_lastE2ESequenceNumber++;
	_tmpMessage.header.flags = flags;
	memcpy(_tmpMessage.data, buf, len);
	return route(&_tmpMessage, sizeof(RoutedMessageHeader)+len);
}

//////////////////////////////////////////////////////////////////////////
bool TreeNode::recvfrom(uint8_t* buf, uint8_t* len, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
    uint8_t _from;
    uint8_t _to;
    uint8_t _id;
    uint8_t _flags;
    uint8_t _len;
    RoutedMessage *_rtMessage;
	
    // process timers and timeouts
    unsigned long now = millis();
	
	if (!isRoot())		// this part only for leaf nodes
	{
		if (_upperBeamTimeoutActive && (now - _upperBeamTimeoutLastTimer) > TN_GLOBAL_BEAM_TIMEOUT) {
			beacon_timeout_trg();
		}
		if (_registrationTimeoutActive && (now - _registrationTimeoutLastTimer) > TN_GLOBAL_REGISTER_TIMEOUT) {
			registration_timeout_trg();
		}
	}
	
	if (_gatewayBeamTimeoutActive && (now - _gatewayBeamLastTimer) > TN_ROOT_BEAM_FREQ) {
		gateway_beacon_send_trg();
	}
	
    _len = sizeof(_tmpMessage);
    // Get the message before its clobbered by the ACK (shared rx and tx buffer in RH
    if (TreeBase::recvfrom((uint8_t*)&_tmpMessage, &_len, &_from, &_to, &_id, &_flags ))
    {
		_rtMessage = &_tmpMessage;
        _flags &= TN_PACKET_TYPE_MASK;
				
        // check if Service message
        if (_flags == TN_PACKET_TYPE_USMG)
        {
	        // check final destination
	        if ( _rtMessage->header.dest == _thisAddress || _rtMessage->header.dest == RH_BROADCAST_ADDRESS)
	        {
		        // we are final destination
		        // broadcast are not repeated

		        if (from)  *from =  _rtMessage->header.source;
		        if (to)    *to =    _rtMessage->header.dest;
		        if (id)    *id =    _rtMessage->header.id;
		        if (flags) *flags = _rtMessage->header.flags;

		        if (*len > TN_MAX_MESSAGE_LEN) {
			        return false;
		        }
		        uint8_t tmpLen = _len - sizeof(RoutedMessageHeader);
		        if (*len < tmpLen) {
			        tmpLen = *len;		// return shortest of the two
		        }
		        memcpy(buf, _rtMessage->data, tmpLen);
		        *len = tmpLen;
		        return true;
	        }
	        else
	        {
		        ///TODO: route message to next gateway
		        route( _rtMessage, *len );
		        return false;
	        }
        } 
		else
        {
            if (_to == RH_BROADCAST_ADDRESS )
            {
				if (isRoot())
				{
					if (_flags == TN_PACKET_TYPE_RNAK && _from == getRoute(_from))
					{
						// remove all route passing by _from
						removeRoutes(_from);
					}
					
				}
				else 
				{
					if ( _flags == TN_PACKET_TYPE_BCN)
					{
						if ( _from == TN_GATEWAY_ROOT)
						{
						#define GATEWAY_SIMULATE
						#ifdef GATEWAY_SIMULATE
							// ignore beacon from root to simulate distance
							if (_thisAddress != 2 && _thisAddress != 3) {
								root_beacon_received_trg();
							}
						#else
							root_beacon_received_trg();
						#endif
						} else {
							gateway_beacon_received_trg();
						}
					}
					if (_flags == TN_PACKET_TYPE_RNAK && _from == _GatewayId)
					{
						registration_NAK_trg();
					}
					if (_flags == TN_PACKET_TYPE_RNAK && _from == getRoute(_from))
					{
						// remove all route passing by _from
						removeRoutes(_from);
					}

				}
            }
            else if (_to == _thisAddress)
            {
				if (isRoot())
				{
					if (_flags == TN_PACKET_TYPE_RREQ)
					{
						debugln(F("ROOT process_registration_request_evt"));
						setLeaf(headerFrom());
						TreeBase::sendto(NULL, 0, headerFrom(), TN_PACKET_TYPE_RACK);
					}
					if (_flags == TN_PACKET_TYPE_RUPD)
					{
						debugln(F("ROOT process_registration_update_evt"));
						updateLeaf( headerFrom(), (uint8_t*)&_tmpMessage);
					}
				}
				else
				{
					if (_flags  == TN_PACKET_TYPE_RACK)
					{
						registration_ACK_trg();
					}
					if (_flags == TN_PACKET_TYPE_RNAK)
					{
						registration_NAK_trg();
					}
					if (_flags == TN_PACKET_TYPE_RREQ)
					{
						registration_request_trg();
					}
					if (_flags == TN_PACKET_TYPE_RUPD)
					{
						registration_update_trg();
					}
				}
            }
            
        }
    }
    // No message for available
    return false;
}

//////////////////////////////////////////////////////////////////////////
bool TreeNode::route(RoutedMessage* message, uint8_t messageLen)
{
	uint8_t next_hop;
	if (message->header.dest == RH_BROADCAST_ADDRESS)
	{
		return false;
	}
	next_hop = getRoute(message->header.dest);
	if ( next_hop != TN_NOT_LEAF )
	{
		return TreeBase::sendto((uint8_t*)message, messageLen, next_hop, TN_PACKET_TYPE_USMG);
	}
	return false;
}

/*
bool TreeNode::recvfromTimeout(uint8_t* buf, uint8_t* len, uint16_t timeout, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
    unsigned long starttime = millis();
    while ((millis() - starttime) < timeout)
    {
        if (recvfromAck(buf, len, from, to, id, flags))
            return true;
        YIELD;
    }
    return false;
}
*/

//////////////////////////////////////////////////////////////////////////
boolean TreeNode::isLeaf(uint8_t node)
{
	if (_leaf_table[node] != TN_NOT_LEAF) {
		return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
/// update the leaf table with data from array
/// modify the _leafTable ONLY if the sender is one of our direct leaf node
/// if bit is set associate the destination with the sender
/// if bit is clear remove association ONLY if it was associated to sender
void TreeNode::updateLeaf(uint8_t sender, uint8_t *newTable)
{
	bool bv;
	if (_leaf_table[sender] != _thisAddress) {
		return;
	}
	for (uint8_t i = 0; i<TN_LEAF_TABLE_LEN; i++) {
		bv = (bool)(newTable[i/8] & (1<<(i%8)));
		
		if (bv) {
			_leaf_table[i] = sender;
		}
		if (!bv && _leaf_table[i] == sender) {
			_leaf_table[i] == TN_NOT_LEAF;
		}
	}
	return;
}

//////////////////////////////////////////////////////////////////////////
void TreeNode::setLeaf(uint8_t node)
{
	_leaf_table[node] = _thisAddress;
}

//////////////////////////////////////////////////////////////////////////
void TreeNode::clearLeaf(uint8_t node)
{
	_leaf_table[node] = TN_NOT_LEAF;
}

//////////////////////////////////////////////////////////////////////////
bool TreeNode::isGateway(void)
{
	for (uint8_t i=0; i<TN_LEAF_TABLE_LEN; i++) {
		if (_leaf_table[i] != TN_NOT_LEAF) {
			return true;
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
uint8_t TreeNode::getRoute(uint8_t to)
{
	if (_leaf_table[to] != TN_NOT_LEAF)
		return _leaf_table[to];	// route to leaf nodes
		
	if (_GatewayId != TN_GATEWAY_NONE && _GatewayId != _thisAddress)
		return _GatewayId;		// route to gateway if present
		
	return TN_NOT_LEAF;			// do not route
}

//////////////////////////////////////////////////////////////////////////
void TreeNode::removeRoutes(uint8_t gateway)
{
	for (uint8_t i=0; i<TN_LEAF_TABLE_LEN; i++) {
		if (_leaf_table[i] == gateway) {
			_leaf_table[i] = TN_NOT_LEAF;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void TreeNode::printState()
{
    unsigned long now = millis();
    
    Serial.println(F("TreeNode state:"));
    if (_upperBeamTimeoutActive) {
        Serial.print(F("   BeamTimer state: active - "));
        Serial.print(F("   BeamTimer count: "));
        Serial.println((TN_GLOBAL_BEAM_TIMEOUT) - (now - _upperBeamTimeoutLastTimer));
    } else {
        Serial.println(F("   BeamTimer: inactive"));
    }
    if (_registrationTimeoutActive) {
        Serial.print(F("   RegistrationTimer state: active - "));
        Serial.print(F("   RegistrationTimer count: "));
        Serial.println((TN_GLOBAL_BEAM_TIMEOUT) - (now - _registrationTimeoutLastTimer));
    } else {
        Serial.println(F("   RegistrationTimer: inactive"));
    }
    if (_gatewayBeamLastTimer) {
        Serial.print(F("   GatewayTimer state: active - "));
        Serial.print(F("   GatewayTimer count: "));
        Serial.println((TN_GLOBAL_BEAM_TIMEOUT) - (now - _gatewayBeamLastTimer));
    } else {
        Serial.println(F("   GatewayTimer: inactive"));
    }
    Serial.print(F("   GatewayLogic state:"));
    switch (actualState) {
        case rf_gateway_logic_wait_master_beam:
            Serial.println(F("rf_gateway_logic_wait_master_beam"));
            break;
        case rf_gateway_logic_register_to_master:
            Serial.println(F("rf_gateway_logic_register_to_master"));
            break;
        case rf_gateway_logic_wait_any_beam:
            Serial.println(F("rf_gateway_logic_wait_any_beam"));
            break;
        case rf_gateway_logic_register_to_gateway:
            Serial.println(F("rf_gateway_logic_register_to_gateway"));
            break;
        case rf_gateway_logic_registered:
            Serial.println(F("rf_gateway_logic_registered"));
            break;
            
        default:
            Serial.println(F("undefined"));
            break;
    }
    Serial.print(F("   RootDistance:"));
    Serial.println(_RootDistance);
    Serial.print(F("   Gateway ID:"));
    Serial.println(_GatewayId);
	Serial.print(F("   Leaf Table: "));
	for (int i=0; i<TN_MAX_DEVICES; i++) {
		if ( isLeaf(i) )
		{
			Serial.print(i);
			Serial.print(' ');
		}
	}
	Serial.println();

    TreeBase::printState();
    
}

