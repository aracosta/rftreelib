// TreeBase.cpp
//
// Define TreeBase datagram protocol
//
// Author: Matteo Costa
// Copyright (C) 2015 Matteo Costa

#define _DEBUG

#include "TreeBase.h"


TreeBase::TreeBase(RHGenericDriver& driver): _driver(driver)
{
    _retransmissions = 0;
    _lastSequenceNumber = 0;
    _timeout = TN_DEFAULT_TIMEOUT;
    _retries = TN_DEFAULT_RETRIES;
    _TxQueueHead = 0;
    _TxQueueTail = 0;
}

bool TreeBase::init()
{
    bool ret = _driver.init();
    if (ret)
    {
        setThisAddress(_thisAddress);
        _driver.setPromiscuous(1);
    }

    return ret;
}

////////////////////////////////////////////////////////////////////
bool TreeBase::sendto(uint8_t* buf, uint8_t len, uint8_t address, uint8_t pkt_type)
{
    if (len > TN_MAX_MESSAGE_LEN)
        return false;

    if ( (_TxQueueHead+1)%TN_MAX_TXQUEUE_LEN != _TxQueueTail )
    {
        _TxQueue[_TxQueueHead]._To = address;
        _TxQueue[_TxQueueHead]._From = _thisAddress;
        _TxQueue[_TxQueueHead]._Id = ++_lastSequenceNumber;
        _TxQueue[_TxQueueHead]._Flags = pkt_type;
        if (buf) {
            _TxQueue[_TxQueueHead]._len = len;
            memcpy(_TxQueue[_TxQueueHead]._payload, buf, len);
        } else {
            _TxQueue[_TxQueueHead]._len = 1;    // dummy payload for some radio
            _TxQueue[_TxQueueTail]._payload[0] = '0';
        }
        _TxQueue[_TxQueueTail]._state = queued;
        _TxQueueHead = (_TxQueueHead+1)%TN_MAX_TXQUEUE_LEN;
        return true;
    } else {
        debugln(F("Queue full"));
        return false;
    }
}

////////////////////////////////////////////////////////////////////
void TreeBase::acknowledge(uint8_t from, uint8_t to, uint8_t id, uint8_t flags)
{
    //TODO: use a dedicate Ack queue
    //TODO: invert to<->from in ack reply
	if (to == RH_BROADCAST_ADDRESS)	// never Ack to broadcast
		return;
		
    if ( (_TxQueueHead+1)%TN_MAX_TXQUEUE_LEN != _TxQueueTail )
    {
        _TxQueue[_TxQueueHead]._To = from;
        _TxQueue[_TxQueueHead]._From = to;
        _TxQueue[_TxQueueHead]._Id = id;
        _TxQueue[_TxQueueHead]._Flags = flags | TN_FLAGS_ACK;
        _TxQueue[_TxQueueHead]._len = 1;
        _TxQueue[_TxQueueTail]._payload[0] = 'A';
        _TxQueue[_TxQueueTail]._state = queued;

        _TxQueueHead = (_TxQueueHead+1)%TN_MAX_TXQUEUE_LEN;
    }
    return;
}

////////////////////////////////////////////////////////////////////
/*
bool TreeBase::repeat(uint8_t flags)
{
    // must check if connected to a root or gateway
    if (_RootDistance == TN_NO_ROOT_DISTANCE) {
        return false;
    }
    
    if ( (_TxQueueHead+1)%TN_MAX_TXQUEUE_LEN != _TxQueueTail )
    {
        _TxQueue[_TxQueueHead]._To = headerTo();
        _TxQueue[_TxQueueHead]._From = headerFrom();
        _TxQueue[_TxQueueHead]._Id = headerId();
        _TxQueue[_TxQueueHead]._Flags = flags;
        _TxQueue[_TxQueueHead]._len = _msgLen;
        memcpy(_TxQueue[_TxQueueHead]._payload, _msgBuffer, _msgLen);
        _TxQueue[_TxQueueTail]._state = queued;

        _TxQueueHead = (_TxQueueHead+1)%TN_MAX_TXQUEUE_LEN;
        return true;
    } else {
        debugln(F("Queue full"));
        return false;
    }
}
*/

////////////////////////////////////////////////////////////////////
//bool TreeBase::recv()
bool TreeBase::recvfrom(uint8_t* buf, uint8_t* len, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
    uint8_t _from;
    uint8_t _to;
    uint8_t _id;
    uint8_t _flags;
    
    // Get the message before its clobbered by the ACK (shared rx and tx buffer in RH
    if (_driver.recv(buf, len))
    {
        // DEBUG CODE
        printDataReceived(buf, *len);
        
        _from =  headerFrom();
        _to =    headerTo();
        _id =    headerId();
        _flags = headerFlags();
        
        
        if (_flags & TN_FLAGS_ACK) 
		{
            if (_TxQueueHead != _TxQueueTail)  // check if ACK to process
            {
                if (_TxQueue[_TxQueueTail]._To == _from &&
                    _TxQueue[_TxQueueTail]._From == _to &&
                    _TxQueue[_TxQueueTail]._Id == _id &&
                    _TxQueue[_TxQueueTail]._Flags == (_flags & ~TN_FLAGS_ACK)
                    ) {
                        _TxQueueTail = (_TxQueueTail+1)%TN_MAX_TXQUEUE_LEN;
                }
            }
            return false;   // never ACK an ACK
        }
        
        
        if (_to == thisAddress() || _to == RH_BROADCAST_ADDRESS)      // check if to me or broadcast and handle to upper level
        {
            acknowledge(_from, _to, _id, _flags);
            if (_seenIds[_from] == _id)    // check duplicate receive
            {
                return false;
            }
            _seenIds[_from] = _id;
		    if (*len > TN_MAX_MESSAGE_LEN) {
			    return false;
		    }

		    if (from)  *from =  headerFrom();
		    if (to)    *to =    headerTo();
		    if (id)    *id =    headerId();
		    if (flags) *flags = headerFlags();
            return true;
        }
#if 0        
        if ( (_flags & TN_FLAGS_PACKET_TYPE_MASK) == TN_PACKET_TYPE_USMG )     // check if USMG for gateway function
        {
            if ( isLeaf(_to) )
            {
                // from upper gateway to leaf
                if ( ((_flags & TN_FLAGS_DIRECTION_MASK) == TN_FLAGS_DIRECTION_ROOT) && (((_flags & TN_FLAGS_ROOT_DISTANCE_MASK)-1) == _RootDistance) ){
                    acknowledge(_from, _to, _id, _flags);
                    if (_seenIds[_from] == _id)    // check duplicate receive
                    {
                        return false;
                    }
                    repeat( (_flags & TN_FLAGS_PACKET_TYPE_MASK) | TN_FLAGS_DIRECTION_LEAF | _RootDistance );
                    _seenIds[_from] = _id;
                }
                else
                // from lower gateway to leaf
                if ( ((_flags & TN_FLAGS_DIRECTION_MASK) == TN_FLAGS_DIRECTION_LEAF) && (((_flags & TN_FLAGS_ROOT_DISTANCE_MASK)+1) == _RootDistance) )
                {
                    acknowledge(_from, _to, _id, _flags);
                    if (_seenIds[_from] == _id)    // check duplicate receive
                    {
                        return false;
                    }
                    repeat( (_flags & TN_FLAGS_PACKET_TYPE_MASK) | TN_FLAGS_DIRECTION_LEAF | _RootDistance );
                    _seenIds[_from] = _id;
                }
            }
            else if ( isLeaf(_from) )
            {
                if ( ((_flags & TN_FLAGS_DIRECTION_MASK) == TN_FLAGS_DIRECTION_ROOT) && (((_flags & TN_FLAGS_ROOT_DISTANCE_MASK)-1) == _RootDistance) )
                {
                    acknowledge(_from, _to, _id, _flags);
                    if (_seenIds[_from] == _id)    // check duplicate receive
                    {
                        return false;
                    }
                    repeat( (_flags & TN_FLAGS_PACKET_TYPE_MASK) | TN_FLAGS_DIRECTION_LEAF | _RootDistance );
                    _seenIds[_from] = _id;
                }
            }
            return false;
        }
        else    // not for me and PACKET_TYPE != USMG. return to upper layer for processing. No Ack for this messages
        {
            _seenIds[_from] = _id;
            return true;
        }
#endif
    }
    else
    {
        // no message received, process TxQueue
        processTx();
        return false;
    }
	return false;
}

////////////////////////////////////////////////////////////////////
void TreeBase::processTx()
{
    unsigned long now = millis();

    // wait TX_DELAY before next transmission
    if (_TxQueueHead != _TxQueueTail)
    {
        if (_TxQueue[_TxQueueTail]._state == queued) {
	        _delayTxMillis = millis();
			_TxQueue[_TxQueueTail]._state = waiting;
			_delayTxTimeout = random(70,210) ;
			_retriesDone = 0;
			return;
		}
		
        if (_TxQueue[_TxQueueTail]._state == sent) {
	        if ( (now - _sentMillis) > _timeout)
	        {
				if (_retriesDone++ < _retries)
				{
					debug(F("Resending message at "));
					debugln(_TxQueueTail);
			        _delayTxMillis = millis();
			        _TxQueue[_TxQueueTail]._state = waiting;
					_delayTxTimeout = random(70,210) ;
				} else {
					debug(F("Timeout message at "));
					debugln(_TxQueueTail);
					_TxQueue[_TxQueueTail]._state = empty;
					_TxQueueTail = (_TxQueueTail+1)%TN_MAX_TXQUEUE_LEN;
				}
	        }
	        return;
        }

        if (_TxQueue[_TxQueueTail]._state == waiting) {
			if ( (now - _delayTxMillis) < _delayTxTimeout)
			{
				return;
			}
		}

        printDataSent();
        
        setHeaderFrom(_TxQueue[_TxQueueTail]._From);
        setHeaderTo(_TxQueue[_TxQueueTail]._To);
        setHeaderFlags(_TxQueue[_TxQueueTail]._Flags, TN_FLAG_RESET);
        setHeaderId(_TxQueue[_TxQueueTail]._Id);
        _driver.send(_TxQueue[_TxQueueTail]._payload, _TxQueue[_TxQueueTail]._len);
        if ( (_TxQueue[_TxQueueTail]._To == RH_BROADCAST_ADDRESS) || (_TxQueue[_TxQueueTail]._Flags & TN_FLAGS_ACK))
        {
            _TxQueue[_TxQueueTail]._state = empty;
            _TxQueueTail = (_TxQueueTail+1)%TN_MAX_TXQUEUE_LEN; // broadcast and ack have no ack or retry
        } else {
            _TxQueue[_TxQueueTail]._state = sent;
			///TODO: start the timeout when finished sending
            _sentMillis = millis();
        }
        return;
    }
    return;
}

void TreeBase::setThisAddress(uint8_t thisAddress)
{
    _driver.setThisAddress(thisAddress);
    // Use this address in the transmitted FROM header
    setHeaderFrom(thisAddress);
    _thisAddress = thisAddress;
}

void TreeBase::setTimeout(uint16_t timeout)
{
    _timeout = timeout;
}

void TreeBase::setRetries(uint8_t retries)
{
    _retries = retries;
}

uint8_t TreeBase::retries()
{
    return _retries;
}

///
bool TreeBase::available()
{
    return _driver.available();
}

void TreeBase::waitAvailable()
{
    _driver.waitAvailable();
}

bool TreeBase::waitPacketSent()
{
    return _driver.waitPacketSent();
}

bool TreeBase::waitPacketSent(uint16_t timeout)
{
    return _driver.waitPacketSent(timeout);
}

bool TreeBase::waitAvailableTimeout(uint16_t timeout)
{
    return _driver.waitAvailableTimeout(timeout);
}

uint8_t TreeBase::thisAddress()
{
    return _thisAddress;
}

void TreeBase::setHeaderTo(uint8_t to)
{
    _driver.setHeaderTo(to);
}

void TreeBase::setHeaderFrom(uint8_t from)
{
    _driver.setHeaderFrom(from);
}

void TreeBase::setHeaderId(uint8_t id)
{
    _driver.setHeaderId(id);
}

void TreeBase::setHeaderFlags(uint8_t set, uint8_t clear)
{
    _driver.setHeaderFlags(set, clear);
}

uint8_t TreeBase::headerTo()
{
    return _driver.headerTo();
}

uint8_t TreeBase::headerFrom()
{
    return _driver.headerFrom();
}

uint8_t TreeBase::headerId()
{
    return _driver.headerId();
}

uint8_t TreeBase::headerFlags()
{
    return _driver.headerFlags();
}

// data print function for debug
void TreeBase::printDataReceived(uint8_t *buf, uint8_t len)
{
    uint8_t v;
    uint8_t _flags;

    _flags = headerFlags();
    
    if (_flags & TN_FLAGS_ACK) {
        Serial.print(F("RX-> Ack [S:"));
    } else {
        Serial.print(F("RX->     [S:"));
    }
    Serial.print(headerFrom(), HEX);
    Serial.print(F(" D:"));
    Serial.print(headerTo(), HEX);
    Serial.print(F(" Id:"));
    Serial.print(headerId(), HEX);
    Serial.print(F(" RD:"));
    //Serial.print(_flags & TN_FLAGS_ROOT_DISTANCE_MASK);
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_BCN)
    {
        Serial.print(F(" BCN "));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RACK)
    {
        Serial.print(F(" RACK"));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RNAK)
    {
        Serial.print(F(" RNAK"));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RREQ)
    {
        Serial.print(F(" RREQ"));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RUPD)
    {
        Serial.print(F(" RUPD"));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_USMG)
    {
        Serial.print(F(" USMG"));
    }

    Serial.print(']');

    Serial.print('(');
    Serial.print(len, HEX);
    Serial.print(')');

    for (uint8_t i = 0; i < len; i++) {
        v = (uint8_t)buf[i];
        Serial.print(' ');
        Serial.print(v, HEX);
    }
    Serial.print(F("   [RX_RSSI:"));
    Serial.print(_driver.lastRssi());
    Serial.println(']');
    return;
}

void TreeBase::printDataSent(void)
{
    uint8_t v;
    uint8_t _flags;
    
    _flags = _TxQueue[_TxQueueTail]._Flags;
    
    if (_flags & TN_FLAGS_ACK) {
        Serial.print(F(" TX-> Ack [S:"));
    } else {
        Serial.print(F(" TX->     [S:"));
    }
    Serial.print(_TxQueue[_TxQueueTail]._From, HEX);
    Serial.print(F(" D:"));
    Serial.print(_TxQueue[_TxQueueTail]._To, HEX);
    Serial.print(F(" Id:"));
    Serial.print(_TxQueue[_TxQueueTail]._Id, HEX);
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_BCN)
    {
        Serial.print(F(" BCN "));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RACK)
    {
        Serial.print(F(" RACK"));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RNAK)
    {
        Serial.print(F(" RNAK"));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RREQ)
    {
        Serial.print(F(" RREQ"));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_RUPD)
    {
        Serial.print(F(" RUPD"));
    }
    if ((_flags & TN_PACKET_TYPE_MASK) == TN_PACKET_TYPE_USMG)
    {
        Serial.print(F(" USMG"));
    }
    //if (_flags & TN_FLAGS_DIRECTION_LEAF) {
        //Serial.print(F(" l"));
    //} else {
        //Serial.print(F(" r"));
    //}
    Serial.print(']');
    
    Serial.print('(');
    Serial.print(_TxQueue[_TxQueueTail]._len, HEX);
    Serial.print(')');
    
    for (uint8_t i = 0; i < _TxQueue[_TxQueueTail]._len; i++) {
        v = (uint8_t)_TxQueue[_TxQueueTail]._payload[i];
        Serial.print(' ');
        Serial.print(v, HEX);
    }
    Serial.println();
    return;
}

void TreeBase::printState()
{
    uint8_t i;
    
    Serial.println(F("TreeBase state:"));
    Serial.print(F("   NodeId:"));
    Serial.println(thisAddress());
    Serial.print(F("   TxQueueHead:"));
    Serial.println(_TxQueueHead);
    Serial.print(F("   TxQueueTail:"));
    Serial.println(_TxQueueTail);
}


