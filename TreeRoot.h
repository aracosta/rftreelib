// TreeNode.h
// Author: Matteo Costa
// Copyright (C) 2015 Matteo Costa

#ifndef TreeRoot_h
#define TreeRoot_h

#include <RHGenericDriver.h>
#include "TreeBase.h"


/////////////////////////////////////////////////////////////////////
/// \class RHDatagram RHDatagram.h <RHDatagram.h>
/// \brief Manager class for addressed, unreliable messages
///
/// Every RHDatagram node has an 8 bit address (defaults to 0).
/// Addresses (DEST and SRC) are 8 bit integers with an address of RH_BROADCAST_ADDRESS (0xff) 
/// reserved for broadcast.
///
/// \par Media Access Strategy
///
/// RHDatagram and the underlying drivers always transmit as soon as sendto() is called.
///
/// \par Message Lengths
///
/// Not all Radio drivers supported by RadioHead can handle the same message lengths. Some radios can handle
/// up to 255 octets, and some as few as 28. If you attempt to send a message that is too long for 
/// the underlying driver, sendTo() will return false and will not transmit the message. 
/// It is the programmers responsibility to make
/// sure that messages passed to sendto() do not exceed the capability of the radio. You can use the 
/// *_MAX_MESSAGE_LENGTH definitions or driver->maxMessageLength() to help.
///
/// \par Headers
///
/// Each message sent and received by a RadioHead driver includes 4 headers:
/// -TO The node address that the message is being sent to (broadcast RH_BROADCAST_ADDRESS (255) is permitted)
/// -FROM The node address of the sending node
/// -ID A message ID, distinct (over short time scales) for each message sent by a particilar node
/// -FLAGS A bitmask of flags. The most significant 4 bits are reserved for use by RadioHead. The least
/// significant 4 bits are reserved for applications.
class TreeRoot : public TreeBase
{
public:
    /// Constructor. 
    /// \param[in] driver The RadioHead driver to use to transport messages.
    /// \param[in] thisAddress The address to assign to this node. Always 0
    TreeRoot(RHGenericDriver& driver);

    /// Initialise this instance and the 
    /// driver connected to it.
    bool init();

    /// Sends a message to the node(s) with the given address
    /// RH_BROADCAST_ADDRESS is a valid address which will cause the message
    /// to be accepted by all RHDatagram nodes within range.
    /// \param[in] buf Pointer to the binary message to send
    /// \param[in] len Number of octets to send (> 0)
    /// \param[in] address The address to send the message to.
    /// \return true if the message not too loing fot eh driver, and the message was transmitted.
    bool sendto(uint8_t* buf, uint8_t len, uint8_t address);

    /// Turns the receiver on if it not already on.
    /// If there is a valid message available for this node, copy it to buf and return true
    /// The SRC address is placed in *from if present and not NULL.
    /// The DEST address is placed in *to if present and not NULL.
    /// If a message is copied, *len is set to the length.
    /// You should be sure to call this function frequently enough to not miss any messages
    /// It is recommended that you call it in your main loop.
    /// \param[in] buf Location to copy the received message
    /// \param[in,out] len Pointer to available space in buf. Set to the actual number of octets copied.
    /// \param[in] from If present and not NULL, the referenced uint8_t will be set to the FROM address
    /// \param[in] to If present and not NULL, the referenced uint8_t will be set to the TO address
    /// \param[in] id If present and not NULL, the referenced uint8_t will be set to the ID
    /// \param[in] flags If present and not NULL, the referenced uint8_t will be set to the FLAGS
    /// (not just those addressed to this node).
    /// \return true if a valid message was copied to buf
    bool recvfrom(uint8_t* buf, uint8_t* len, uint8_t* from = NULL, uint8_t* to = NULL, uint8_t* id = NULL, uint8_t* flags = NULL);
    
    // print the internal state for debugging
    void printState();
    
protected:
    /// Sends a message to the node(s) with the given address
    /// RH_BROADCAST_ADDRESS is a valid address which will cause the message
    /// to be accepted by all RHDatagram nodes within range.
    /// \param[in] buf Pointer to the binary message to send
    /// \param[in] len Number of octets to send (> 0)
    /// \param[in] address The address to send the message to.
    /// \param[in] pkt_type The packet type to put in FLAGS
    /// \return true if the message not too loing fot eh driver, and the message was transmitted.
    bool sendto(uint8_t* buf, uint8_t len, uint8_t address, uint8_t pkt_type);
    
    void beacon_timeout_start_evt(void);
    void beacon_timeout_stop_evt(void);
    void root_beacon_evt(void);

private:
    unsigned long _beamLastTimer;
    uint8_t _beamActive;
    

};

#endif
